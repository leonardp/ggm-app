import zmq
import asyncio
from ggm.lib.kernel import GK
from ggm.lib.kontext import Kontext

from random import randint
from datetime import datetime

from pprint import pprint
from time import sleep

class Dummy(GK):
    def __init__(self, *args, **kwargs):
        GK.__init__(self, *args, **kwargs)
        self.glog.info(f'Starting Dummy Kernel! config_example: {kwargs["test_key"]}')

    async def gen_rand_data(self, res):
        res_name = res
        tags = {}
        # tags are string only!
        tags['tags'] = 'testing,dummy'
        tags['unit'] = 'False'
        while True:
            await asyncio.sleep(1.0)
            raw = randint(1,10)
            self.glog.debug(f'{res_name} - data producer')
            await self.send_data(self.dev_id, res_name, {'rand': raw}, tags)

    async def timed_to_stop(self):
        end_time = self.loop.time() + 3.0
        while True:
            try:
                self.glog.debug(datetime.now())
            except Exception as e:
                print(e)
            if (self.loop.time() + 1.0) >= end_time:
                break
            await asyncio.sleep(1)

    async def simulate_hang(self, zzz):
        while True:
            await asyncio.sleep(10.0)
            sleep(zzz)


def dummy_kernel_process(kcfg):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    kcfg['context'] = Kontext()
    kcfg['loop'] = loop
    
    dummy = Dummy(**kcfg)
    for name in kcfg['rand_data']:
        loop.create_task(dummy.gen_rand_data(name))
    #loop.create_task(dummy.timed_to_stop())
    #loop.create_task(dummy.simulate_hang(1.5))
    dummy.start()
