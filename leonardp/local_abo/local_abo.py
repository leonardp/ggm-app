"""
    Copyright (c) 2019 Contributors as noted in the AUTHORS file
    This file is part of ggm, the GILGAMESH core engine in Python.
    ggm is free software; you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License (GPL) as published
    by the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    As a special exception, the Contributors give you permission to link
    this library with independent modules to produce an executable,
    regardless of the license terms of these independent modules, and to
    copy and distribute the resulting executable under terms of your choice,
    provided that you also meet, for each linked independent module, the
    terms and conditions of the license of that module. An independent
    module is a module which is not derived from or based on this library.
    If you modify this library, you must extend this exception to your
    version of the library.
    ggm is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
    License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import zmq
import asyncio
import sqlite3

from ggm.lib.kernel import GK
from ggm.lib.kontext import Kontext
from ggm.lib.auth import GAuthClient

import time
from pprint import pprint

class LocalAbo(GAuthClient, GK):
    def __init__(self, *args, **kwargs):
        GK.__init__(self, *args, **kwargs)

        loader = GAuthClient(keys_dir=kwargs['keys_dir'], uplink=kwargs['uplink'])

        self.DB = "ipc:///tmp/local_abo"

        self.loop.create_task(self.abo_task())
        self.loop.create_task(self.query_task())

    async def query_task(self):
        """
        task for answering queries
        """
        pass

    async def abo_task(self):
        pass

    def append_data(self, dev_id, measurement, data):
        """
        """
        pass

def local_abo_process(kcfg):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    kcfg['context'] = Kontext()
    kcfg['loop'] = loop

    abo = LocalAbo(**kcfg)
    abo.start()
