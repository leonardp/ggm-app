import os
import zmq
import asyncio
from ggm.lib.kernel import GK
from ggm.lib.kontext import Kontext
from pathlib import Path
import iio
import RPi.GPIO as gpio
import re

class Dispenser(GK):
    def __init__(self, *args, **kwargs):
        GK.__init__(self, *args, **kwargs)

        self.con = iio.Context()
        self.devs = self.con.devices
        #Definitions
        self.IIO_PATH = Path('/sys/bus/iio/devices')
        self.SLEEP_TIME = 3
        self.PWM_FREQ = 5000
        self.DUTY_CYCLE = 100

        H1_PIN = 12
        H2_PIN = 13
        S1_PIN = 17
        S2_PIN = 18
        ADC1 = 1
        ADC2 = 0
        self.adc = self.con.find_device('ads1015')
        self.adc_map = {
            1: {'pwm_pin': H1_PIN, 'sense_pin': S1_PIN, 'adc_ch': ADC1},
            2: {'pwm_pin': H2_PIN, 'sense_pin': S2_PIN, 'adc_ch': ADC2}
        }
        
        self.sensor_attrs = {
            'sgp30': {'concentration_co2': '', 'concentration_voc':''},
            'ccs811': {'concentration_co2': 'ppm', 'concentration_voc': 'ppm', 'current': 'mA', 'voltage': 'mV'},
            'bme680': {'humidityrelative': 'percent', 'pressure': 'mbar', 'resistance': 'ohm', 'temp': 'mdegree_celsius'}
        }

        #Setup Pins
        gpio.setmode(gpio.BCM)
        pins = [H1_PIN,H2_PIN,S1_PIN,S2_PIN]
        for pin in pins:
            gpio.setup(pin,gpio.OUT)

        for a in self.adc_map.keys():
            self.adc_map[a]['pwm'] = gpio.PWM(self.adc_map[a]['pwm_pin'],self.PWM_FREQ)
            self.adc_map[a]['pwm'].start(100-self.DUTY_CYCLE)
            ch = self.adc.find_channel(f'voltage{self.adc_map[a]["adc_ch"]}')
            self.adc_map[a]['scale'] = float(ch.attrs['scale'].value)

        self.sensor_list = []
        for d in self.devs:
            if not d.name == 'ads1015':
                self.sensor_list.append(self.make_sensor_id(d))
            
    def make_sensor_id(self,dev):
        """
        make a sensor tuple consisting if the iio device id, the sensor type, and a unique id ("<type>-<i2c-bus>")
        """
        dev_id = dev.id
        dev_type = dev.name
        with open(self.IIO_PATH / dev.id / 'uevent') as f:
            lines = f.readlines()
        dev_i2c = 'i2c' + re.search('i2c\@([0,1])',lines[-3]).groups()[0]
        dev_unique = f'{dev_type}_{dev_i2c}'
        sensor_desc = (dev_id,dev_type,dev_unique)
        return sensor_desc
        

    #Readout functions
    def adc2r(self,adc,scale):
        """
        return the resistance as calculated from adc values and given gain
        """
        r_l = 3.9e6 #fixed resistance
        u_l = 1.225 #fixed voltage across r_l
        i_l = u_l/r_l
        r = scale*adc/i_l/1e3 #mV
        return r


    async def read_tgs(self, channel):
        """
        read analogue sensor and send data_dict
        """
        tags=dict()
        tags['unit'] = 'ohm'

        while True:
            await asyncio.sleep(self.SLEEP_TIME)

            gpio.output(self.adc_map[channel]['sense_pin'],0)
            ch = self.adc.find_channel(f'voltage{channel}')
            try:
                val = float(ch.attrs['raw'].value)
            except:
                self.glog.error(f'tgs{channel} failed hard')
                continue
            scale = self.adc_map[channel]['scale']
            res = self.adc2r(val,scale)
            gpio.output(self.adc_map[channel]['sense_pin'],1)

            sensor_id = f'TGS8100_{channel}'
            data_dict = {'res': res}

            #self.glog.error(sensor_id,data_dict)

            await self.send_data(self.dev_id, sensor_id,data_dict , tags)

    async def read_sensor(self, sensor_desc):
        """
        read digital sensor and send data_dict
        """
        tags={}
        channel_dict = {}
        dev = self.con.find_device(sensor_desc[0])
        sensor_id = sensor_desc[2]
        attrs_dict = self.sensor_attrs[sensor_desc[1]]
        for attr in attrs_dict.keys():
            tags[f'{attr}_unit'] = attrs_dict[attr]
            channel_dict[attr] = dev.find_channel(attr)

        while True:
            await asyncio.sleep(self.SLEEP_TIME)
            data_dict = {}
            for attr in attrs_dict:
                if not "ccs811" in sensor_desc:
                    try:
                        data_dict[attr]= channel_dict[attr].attrs['input'].value
                    except:
                        self.glog.error(f'{sensor_id} iio failed hard')
                        continue
                else:
                    try:
                        data_dict[attr]= channel_dict[attr].attrs['raw'].value
                    except:
                        self.glog.error(f'{sensor_id} iio failed hard')
                        continue

            #self.glog.debug(f'{sensor_id}: {data_dict}')
            await self.send_data(self.dev_id, sensor_id,data_dict , tags)

        

def dispenser_process(kcfg):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    kcfg['context'] = Kontext()
    kcfg['loop'] = loop
    
    dis = Dispenser(**kcfg)
    #Create a loop for each analogue sensor
    for a in dis.adc_map.keys():
        loop.create_task(dis.read_tgs(a))
    #Create a loop for each digital sensor
    for sensor in dis.sensor_list:
        loop.create_task(dis.read_sensor(sensor))
    dis.start()
